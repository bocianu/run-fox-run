//
// ;*** define your project wide constants here
// 
// ;*** I like to keep memory locations at top of this file
//

//FREE_BOTTOM = $6000;  // and sometimes I like to do some relative addressing
//CHARSET_ADDRESS = FREE_BOTTOM;


DISPLAY_LIST_ADDRESS = $6000;
PMG_ADDRESS = $6000;
TILES_ADDRESS = $6800; 
DIRT_ADDRESS = $6900;
CHARSET_ADDRESS = $6C00;
VIDEO_RAM_ADDRESS = $7000; 
SPRITES_ADDRESS = $8000;
DIGITS_ADDRESS = $8200;

//FOX_LEFT = $8400;
//FOX_RIGHT = $8800;
//FOX_LEFT_DL = $6100;
//FOX_RIGHT_DL = $6200;

	scr = $4A00;

	fnt0 = $5000;
	fnt1 = $5400;
	fnt2 = $5800;

RMT_PLAYER_ADDRESS = $8800;
RMT_MODULE_ADDRESS = $9000;

// ;*** and here goes all other stuff

NONE = $ff;


TILES_NUM = 10;
FOX_Y = 196;
FOX_FRAMES_COUNT = 2;
FOX_HEIGHT = 24;
DMG_DELAY = 3;
CAMERA_LEFT = 64;
CAMERA_TOP = 24;
DIRT_COUNT = 80;

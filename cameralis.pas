program madStrap;
uses atari, crt, rmt, joystick, g9pp; 
const
{$i const.inc}
{$r resources.rc}
{$i interrupts.inc}

var
    fcol,b: byte;
    treelevel:byte;
    w: word;
    s: TString;
    yoffset: word;
    lmsAddr:word absolute DISPLAY_LIST_ADDRESS + 3;
    vram:array [0..0] of byte absolute VIDEO_RAM_ADDRESS;
    dlistart:pointer;
    msx: TRMT;
    musicOn: boolean;
    pconsol: byte;
    //oldvbl,oldsdli:pointer;
    //strings:array [0..0] of word absolute STRINGS_ADDRESS;

    tileSizes: array [0..TILES_NUM-1] of byte = (2,2,2,3,3,4,4,5,5,5);
    tiles: array [0..TILES_NUM-1] of pointer absolute TILES_ADDRESS;
    dirt: array [0..DIRT_COUNT-1] of byte = (0,1,2,3,4,5,6,7,8,9,
        $81,$82,$83,$84,$85,$86,$87,$88,$89,$90,
        $91,$92,$93,$94,$95,0,0,0,0,0,
        0,1,2,3,4,5,6,7,8,9,
        0,1,2,3,4,5,6,7,8,9,
        0,1,2,3,4,5,6,7,8,9,
        0,1,2,3,4,5,6,7,8,9,
        0,0,0,0,0,0,0,0,0,0);

    gameoverchars0: array [0..17] of byte = (
        %01000100,
        %11101110,
        %10101010,
        %10001010,
        %10101110,
        %10101010,
        %11101010,
        %01101010,
        %00000000,
        %00000000,
        %01001010,
        %11101010,
        %10101010,
        %10101010,
        %10101010,
        %10101110,
        %11100100,
        %01000100
    );

    gameoverchars1: array [0..17] of byte = (
        %10101110,
        %11101110,
        %11101000,
        %11101100,
        %10101000,
        %10101000,
        %10101110,
        %10101110,
        %00000000,
        %00000000,
        %11101100,
        %11101110,
        %10001010,
        %11001010,
        %10001100,
        %10001010,
        %11101010,
        %11101010
    );


    tile:array[0..0] of byte;
    toprow:byte;
    gencount:byte;
    fox_x:byte;
    fox_frame:byte;
    fox_dir:byte;
    fox_sprites0: array [0..5,0..FOX_HEIGHT-1] of byte absolute SPRITES_ADDRESS;
    fox_sprites1: array [0..5,0..FOX_HEIGHT-1] of byte absolute SPRITES_ADDRESS + 6*FOX_HEIGHT;
    fox_dir_frames: array [0..2,0..FOX_FRAMES_COUNT-1] of byte = ((3,4),(0,1),(2,5));
    frame:byte absolute $14;
    laststrig0:byte;
    fox_turn_count: byte;
    fox_energy: byte;
    hitdelay: byte;
    gameover: boolean;
    digits0: array [0..9, 0..7] of byte absolute DIGITS_ADDRESS;
    digits1: array [0..9, 0..7] of byte absolute DIGITS_ADDRESS+80;
    cameraX: array [0..1] of SMALLINT;
    cameraY: array [0..1] of SMALLINT;
    cameraDir: array [0..1] of byte;
    score:word;
    scoreadd:byte;
    distance:cardinal;
    rows:byte;
    flash:byte;
    fadeout:byte;
    defaultColors:array [0..6] of byte = ($c6,$b2,$a4,$16,$a0,$24,$28);
const 
    dlist: array [0..34] of byte = (
		$C4,lo(scr),hi(scr),
		$84,$84,$84,$84,$84,$84,$84,$84,
		$84,$84,$84,$84,$84,$84,$84,$84,
		$84,$84,$84,$84,$84,$84,$84,$84,
		$84,$84,$84,$84,$04,
		$41,lo(word(@dlist)),hi(word(@dlist))
	);

	fntTable: array [0..29] of byte = (
		hi(fnt0),hi(fnt0),hi(fnt0),hi(fnt0),hi(fnt0),hi(fnt0),hi(fnt0),hi(fnt0),
		hi(fnt0),hi(fnt0),hi(fnt0),hi(fnt1),hi(fnt1),hi(fnt1),hi(fnt1),hi(fnt1),
		hi(fnt1),hi(fnt0),hi(fnt1),hi(fnt1),hi(fnt1),hi(fnt1),hi(fnt2),hi(fnt1),
		hi(fnt0),hi(fnt2),hi(fnt2),hi(fnt2),hi(fnt0),hi(fnt0)
	);

	c0Table: array [0..29] of byte = (
		$00,$00,$00,$00,$00,$00,$00,$00,
		$00,$00,$00,$00,$00,$00,$00,$00,
		$00,$00,$00,$00,$00,$00,$00,$00,
		$00,$00,$00,$00,$00,$00
	);

	c1Table: array [0..29] of byte = (
		$2A,$2A,$2A,$2A,$2A,$2A,$2A,$2A,
		$2A,$2A,$2A,$2A,$2A,$2A,$2A,$2A,
		$2A,$2A,$2A,$2A,$2A,$2A,$2A,$2A,
		$2A,$2A,$2A,$2A,$2A,$2A
	);

	c2Table: array [0..29] of byte = (
		$08,$08,$08,$08,$08,$08,$08,$08,
		$08,$08,$08,$08,$08,$08,$08,$08,
		$08,$08,$08,$08,$08,$08,$08,$08,
		$08,$08,$08,$08,$08,$08
	);

	c3Table: array [0..29] of byte = (
		$0E,$0E,$0E,$0E,$0E,$0E,$0E,$0E,
		$0E,$0E,$0E,$0E,$0E,$0E,$0E,$0E,
		$0E,$0E,$0E,$0E,$0E,$0E,$0E,$0E,
		$0E,$0E,$0E,$0E,$0E,$0E
	);
var
    old_dli, old_vbl: pointer;

procedure vblt; assembler; interrupt;
asm
{
	mva #1 dli.cnt

	mva adr.fntTable chbase
	mva adr.fntTable+1 dli.chbs

	mva adr.c0Table color0
	mva adr.c0Table+1 dli.col0
	mva adr.c1Table color1
	mva adr.c1Table+1 dli.col1
	mva adr.c2Table color2
	mva adr.c2Table+1 dli.col2
	mva adr.c3Table color3
	mva adr.c3Table+1 dli.col3

	mva #$0E colbak

    lda MSX
    ldy MSX+1
    jsr RMT.TRMT.PLAY

	jmp xitvbv
};
end;


procedure dli; assembler; interrupt;
asm
{
	sta rA
	stx rX
	sty rY

	lda #0
chbs	equ *-1

	ldx #0
col0	equ *-1

	ldy #0
col1	equ *-1

	;sta wsync

	sta chbase
	lda #0
col2	equ *-1
	stx color0
	ldx #0
col3	equ *-1
	sty color1
	sta color2
	stx color3

	inc cnt

	ldx #0
cnt	equ *-1

	lda adr.fntTable,x
	sta chbs

	lda adr.c0Table,x
	sta col0

	lda adr.c1Table,x
	sta col1

	lda adr.c2Table,x
	sta col2

	lda adr.c3Table,x
	sta col3

	lda #0
rA	equ *-1
	ldx #0
rX	equ *-1
	ldy #0
rY	equ *-1
};
	end;



procedure ShowFox;
var f:byte;
begin
    if fox_turn_count>0 then f:=fox_dir_frames[2,fox_frame]
    else f:=fox_dir_frames[fox_dir,fox_frame];
    move(@fox_sprites0[f,0],pointer(PMG_ADDRESS+$600+FOX_Y),FOX_HEIGHT);
    move(@fox_sprites1[f,0],pointer(PMG_ADDRESS+$700+FOX_Y),FOX_HEIGHT);
end;

procedure ShowEnergy;
var i,g:byte;
    off:word;
begin
    off := PMG_ADDRESS + $300 + 64;
    for i:=0 to 63 do begin
        g:=$30;
        if i > 63 - fox_energy then g:=$f0;
        poke(off,g);
        inc(off,2);
    end;
end;

procedure addcamera(cx,cy:SMALLINT;dir:byte);
var i:byte;
begin
    for i:=0 to 1 do begin
        if cameraX[i]=0 then begin
            cameraX[i]:=cx;
            cameraY[i]:=cy;
            cameraDir[i]:=dir;
            exit;
        end;
    end;
end;



procedure PutTile(offset:word;t:byte);
var 
    x,y,o:byte;

begin
    tile := pointer(tiles[t]);
    o:=0;
    y := tileSizes[t];
    while y > 0 do begin
        x := tileSizes[t];
        while x > 0 do begin
            vram[offset] := tile[o];
            inc(o);
            dec(x);
            inc(offset);
        end;
        inc(offset,  32 - tileSizes[t]);
        dec(y);
    end;
    
end;    

procedure GenRow(row,num:byte);
var t,x,y,s,dir:byte;
    cy,cx:SMALLINT;
    line:array[0..31] of byte;
    first:boolean;

function isFree(tx,ts:byte):boolean;
begin
    result:=true;
    while ts>0 do begin
        if line[tx]<>0 then exit(false);
        inc(tx);
        dec(ts);
    end;
end;

begin
    first:=true;
    FillByte(@line,32,0);
    Move(pointer(DIRT_ADDRESS+Random($200)),pointer(VIDEO_RAM_ADDRESS+word(row shl 5)),8*32);
    while num>0 do begin
        if first and (rows and 3 = 3) then begin
            dir:=random(2);
            t:=(TILES_NUM - 2) + dir; 
            s:=tileSizes[t];
            first:=false;
            x := 12+Random(8-s);
            y := Random(8-s);
            cy := CAMERA_TOP - ((6-y) shl 3);
            cx := CAMERA_LEFT + ((x + (s shr 1)) shl 2);
            addcamera(cx,cy,dir);
        end else begin
            t:=Random(TILES_NUM-2);
            s:=tileSizes[t];
            x := Random(32-s);
            y := Random(8-s);
        end;
        tile := pointer(tiles[t]);
        if isFree(x,s) then begin
            y := y + row;
            PutTile((y shl 5) + x,t);
            Fillbyte(@line[x],s,1);
        end;
        dec(num);
    end;
    inc(rows);
end;

procedure ShowScore;
var r,d0,d1,d2,d3:byte;
    off:word;
begin
    d0:=score shr 12;
    d1:=(score shr 8) and $f;
    d2:=(score shr 4) and $f;
    d3:=score and $f;
    off:=PMG_ADDRESS+$400+28;
    for r:=0 to 7 do begin
        poke(off, digits0[d0,r] or digits1[d1,r]);
        poke(off+$100, digits0[d2,r] or digits1[d3,r]);
        inc(off);
    end;
end;

procedure ShowGameOver;
var off:word;
    r:byte;
begin
    off:=PMG_ADDRESS+$400+42;
    for r:=0 to 17 do begin
        poke(off, gameoverchars0[r]);
        poke(off+$100, gameoverchars1[r]);
        inc(off);
    end;
end;

procedure Movecameras;
var i:byte;
begin
    for i:=0 to 1 do begin
        if cameraX[i] <> 0 then begin
            if cameraY[i]=196 then begin
                if (cameraDir[i]=1) and (cameraX[i]<fox_x) then flash:=1;
                if (cameraDir[i]=0) and (cameraX[i]>fox_x) then flash:=1;
                if flash=0 then begin
                    scoreadd := 5;
                    asm jsr addScore end;
                    ShowScore;
                end else begin  
                    msx.sfx(3,3,0);
                    msx.sfx(3,7,0);
                end;
            end;
            If cameraY[i]>210 then begin
                cameraX[i] := 0;
            
            end else cameraY[i] := cameraY[i] + 2;
        end;
     end;
end;

procedure TurnFox;
begin
    if fox_dir = 1 then fox_dir := 0 else fox_dir := 1;
    fox_turn_count := 4;
end;

// procedure ShowFoxPhoto(dir:byte);
// begin   
//     pause;
//     if dir=0 then begin 
//         SDLSTL := FOX_RIGHT_DL;
//     end else begin  
//         SDLSTL := FOX_LEFT_DL;
//     end;
//     sdmctl := %00111110;
//     GRACTL := 0;
//     dlistart := @G9Dli;
//     gprior := $41;
    
//     //color4:=$f0;
// end;

procedure SetDefaultColors;
begin
    color0 := defaultColors[0];
    color1 := defaultColors[1];
    color2 := defaultColors[2];
    color3 := defaultColors[3];
    color4 := defaultColors[4];
    pcolr2 := defaultColors[5];
    pcolr3 := defaultColors[6];
end;

procedure GameInit;
begin
    
    hposm2 := 198;
    hposm3 := 199;
    sizem := %00010000;

    SetDefaultColors;

    fox_dir := 0;
    fox_x := 124;
    fox_frame := 0;
    fox_energy := 64;
    hitdelay := 0;
    distance := 0;
    gameover := false;
    score := 0;
    rows := 0;
    treelevel := 0;
    flash := 0;
    fadeout := 0;
    gencount:=0;

    yoffset := 768;
    laststrig0 := strig0;

    FillByte(@cameraX,4,0);
    FillByte(@cameraY,4,0);


    Pause;
    sdmctl := %00111101;
    
    GRACTL := 3;
    SDLSTL := DISPLAY_LIST_ADDRESS;
    dlistart := @dli0;
    PMBASE := Hi(PMG_ADDRESS);
    FillByte(pointer(PMG_ADDRESS+$300),$500,0);
    SetIntVec(iVBL, @vbl);

    SetIntVec(iDLI, @dli0); //dlistart);
    nmien := $c0; // set $80 for dli only (without vbl)
    chbas := Hi(CHARSET_ADDRESS); // set custom charset   

    ShowEnergy;
    ShowScore;
end;


procedure ShowTitle;
begin

        while keypressed do readkey;
        repeat until strig0 = 1;
        msx.Init(9);
    GetIntVec(iVBL, old_vbl);
    GetIntVec(iDLI, old_dli);

    GRACTL:=0;
    sdmctl := byte(normal or enable or missiles or players or oneline);
    sdlstl := word(@dlist);	// ($230) = @dlist, New DLIST Program

    SetIntVec(iVBL, @vblt);
    SetIntVec(iDLI, @dli);

 nmien := $c0;			// $D40E = $C0, Enable DLI

 repeat  until keypressed or (strig0 = 0);

 SetIntVec(iVBL, old_vbl);
 SetIntVec(iDLI, old_dli);
end;

procedure SwitchMusic;
begin
    msx.Stop;
    if musicOn then msx.Init($c)
    else msx.Init(0);
    musicOn := not musicOn;
end;

begin
    FillByte(pointer(PMBASE+$300),$500,0);
    msx.player := pointer(RMT_PLAYER_ADDRESS);
    msx.modul := pointer(RMT_MODULE_ADDRESS);

    Randomize;
    musicOn := true;
    pconsol := consol;

    for w:=0 to $2ff do poke(DIRT_ADDRESS+w,dirt[random(DIRT_COUNT)]);
    move(pointer(DIRT_ADDRESS),pointer(VIDEO_RAM_ADDRESS+$C00),$300);

    repeat

        ShowTitle;

        GameInit;
        if musicOn then msx.Init(0) else msx.Init($c);

        repeat
            if fadeout>0 then begin
                dec(fadeout);
                if fadeout = 1 then begin
                    fadeout := 0;
                    for b:=0 to 4 do begin
                        fcol := peek(708+b);
                        if fcol>defaultColors[b] then begin
                            fadeout := 4;
                            poke(708+b,fcol - 1);
                        end;
                        if fcol = $f then begin
                            poke(708+b,defaultColors[b] or $0f);
                            fadeout := 4;
                        end;
                    end;
                
                end;
            end;

            if flash>0 then begin
                color0 := $0f;
                color1 := $0f;
                color2 := $0f;
                color3 := $0f;
                color4 := $0f;
                flash := 0;
                fadeout := 20;
            end;

            toprow := yoffset shr 3;
            if gencount=0 then begin
                treelevel := rows shr 5;
                if (treelevel>5) then treelevel := 5;        
                treelevel := Random(treelevel+1);
                if toprow>7 then GenRow(toprow-8,treelevel+2);
                gencount:=31;
            end else Dec(gencount);
            vscrol := yoffset and 7;
            lmsAddr := VIDEO_RAM_ADDRESS + ((yoffset shr 3) shl 5);
            
            
            if yoffset = 0 then begin
                yoffset := 768; 
                move(pointer(VIDEO_RAM_ADDRESS),pointer(VIDEO_RAM_ADDRESS+(((yoffset shr 3) shl 5))),768)
            end else yoffset:=yoffset-2;

            hitclr:=0;
            Movecameras;
            
            if frame and 7 = 7 then Inc(fox_frame);
            if fox_frame=FOX_FRAMES_COUNT then fox_frame:=0;
            if fox_turn_count>0 then 
                Dec(fox_turn_count);
        
            if fox_dir = 0 then begin
                Dec(fox_x);
            end else Inc(fox_x);

            if (fox_x < 64) and (fox_dir=0) then TurnFox;
            if (fox_x > 184) and (fox_dir=1) then TurnFox;
            if (strig0 = 0) and (laststrig0 = 1) then TurnFox;
            laststrig0 := strig0;


            ShowFox;
            pause();

            if hitdelay=0 then begin
                if (hposm2 or hposm3) and 3 <> 0 then begin
                    Dec(fox_energy);
                    ShowEnergy;
                    hitdelay:=DMG_DELAY;
                    if fox_energy = 0 then gameover := true;
                end else Inc(distance);
            end else Dec(hitdelay);

            
            if distance and 31 = 31 then begin
                scoreadd := 1;
                asm jsr addScore end;
                ShowScore;
            end;

            if ((consol xor pconsol) and (consol and 4)) = 4 then SwitchMusic;
            pconsol := consol;

        until gameover;
    
        SetDefaultColors;
        ShowGameOver;

        pause(50);
        while keypressed do readkey;
        repeat until strig0 = 1;
         repeat  until keypressed or (strig0 = 0);


    until false;

    


    msx.Play;

end.

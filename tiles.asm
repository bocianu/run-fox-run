    ;dta a(tile2x2_0)
    ;dta a(tile2x2_1)
    ;dta a(tile2x2_2)
    dta a(tile2x2_3)
    dta a(tile2x2_4)
    dta a(tile2x2_5)
    dta a(tile3x3_0)        
    dta a(tile3x3_1)        
    dta a(tile4x4_0)   
    dta a(tile4x4_1)    
    dta a(tile5x5)
    dta a(tile5x5_L)
    dta a(tile5x5_R)
;tile2x2_0
;    dta $0,$0
;    dta $8a,$8b

;tile2x2_1
;    dta $8c,$8d
;    dta $0,$0

;tile2x2_2
;    dta $8e,$8f
;    dta $0,$0

tile2x2_3
    dta $16,$17
    dta $18,$19

tile2x2_4
    dta $1a,$1b
    dta $1c,$1d

tile2x2_5
    dta $1e,$1f
    dta $20,$21

tile3x3_0         
    dta $22,$23,$24
    dta $25,$26,$27
    dta $28,$29,$2a

tile3x3_1         
    dta $2b,$2c,$2d
    dta $2e,$2f,$30
    dta $31,$32,$33

tile4x4_0    
    dta $34,$35,$36,$37
    dta $38,$39,$3a,$3b
    dta $3c,$3d,$3e,$3f
    dta 0,$40,$41,$42

tile4x4_1    
    dta $43,$44,$45,$46
    dta $47,$26,$48,$49
    dta $4a,$4b,$4c,$4d
    dta $4e,$4f,$50,$51

tile5x5
    dta $52,$53,$54,$55,$56
    dta $57,$58,$59,$5a,$5b
    dta $5c,$5d,$59,$5e,$5f
    dta $60,$61,$62,$63,$64
    dta 0,$65,$66,$67,0
    
tile5x5_L
    dta $52,$53,$54,$55,$56
    dta $57,$58,$59,$5a,$5b
    dta $6c,$6d,$6e,$5e,$5f
    dta $60,$61,$62,$63,$64
    dta 0,$65,$66,$67,0

tile5x5_R
    dta $52,$53,$54,$55,$56
    dta $57,$58,$59,$5a,$5b
    dta $5c,$5d,$69,$6a,$6b
    dta $60,$61,$62,$63,$64
    dta 0,$65,$66,$67,0

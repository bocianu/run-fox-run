(* declare your interrupt routines here *)

procedure dli0;assembler;interrupt;
asm {
    pha ; store registers

    lda #$0f
    sta colpm0
    sta colpm1
    sta colpm2
    sta colpm3
    lda #33
    sta $D01B ; gprior
    lda #120
    sta hposp0
    lda #128
    sta hposp1

    mwa #dli1 atari.VDSLST
    
    pla ; restore registers
};
end;

procedure dli1;assembler;interrupt;
asm {
    pha ; store registers

    lda #$0
    sta colpm2
    lda #$bc
    sta colpm3
    mwa #dli2 atari.VDSLST
    
    pla ; restore registers
};
end;

procedure dli2;assembler;interrupt;
asm {
    pha ; store registers

    lda #$fa
    sta colpm3
    mwa #dli3 atari.VDSLST
    
    pla ; restore registers
};
end;

procedure dli3;assembler;interrupt;
asm {
    pha ; store registers

    lda #$28
    sta colpm3
    mwa #dli4 atari.VDSLST
    
    pla ; restore registers
};
end;

procedure dli4;assembler;interrupt;
asm {
    pha ; store registers

    :6 sta wsync
    lda #$24
    sta colpm2
    lda #$28
    sta colpm3
    lda fox_x
    sta hposp2
    sta hposp3
    lda #40
    sta $D01B ; gprior
    mwa #dli0 atari.VDSLST
    
    pla ; restore registers
};
end;



procedure vbl;assembler;interrupt;
asm {
    phr ; store registers
    
;   *** example test routine    
;    mva 20 atari.colbk // blink background
    
;   *** RMT play routine
    lda MSX
    ldy MSX+1
    jsr RMT.TRMT.PLAY

    mwa dlistart atari.VDSLST

    plr ; restore registers
    jmp $E462 ; jump to system VBL handler

.def :addScore
    txa
    pha
    sed
    clc
    lda score
    adc scoreadd
    sta score
    lda score+1
    adc #0
    sta score+1
    cld
    pla 
    tax
    rts
    
};
end;
